#!/bin/bash

. bin/config.inc
. bin/import_functions.inc

SHORT='("2014-2015"|"2015-2016"|"2016-2017"|"2017-2018"|"2018-2019")';

curl -s -X GET "http://$COUCHHOST:$COUCHPORT/$COUCHBASE/_design/configuration/_view/produits?reduce=false" | sed -f bin/unicode2alpha | grep "produits" | sed 's/"declaration\//"\/declaration\//g' | sed 's/\//\\\//g' | sed 's/null/""/' | awk -F ',' '{ print "s/;;\\(" $7 "\\)/;\\1;" $9 "/" }' | grep -E '"[0-9]+"/' | sort | uniq > $TMP/sed_produit_code_cleaned

echo 's/;;\("\/declaration\/certifications\/AOC\/genres\/TRANQ\/appellations\/AJV\/mentions\/DEFAUT\/lieux\/BRI\/couleurs\/rouge\/cepages\/DEFAUT"\)/;\1;"92"/' >> $TMP/sed_produit_code_cleaned
echo 's/;;\("\/declaration\/certifications\/AOC\/genres\/TRANQ\/appellations\/MUS\/mentions\/DEFAUT\/lieux\/AC\/couleurs\/blanc\/cepages\/MEL"\)/;\1;"1040"/' >> $TMP/sed_produit_code_cleaned
echo 's/;;\("\/declaration\/certifications\/AOC\/genres\/TRANQ\/appellations\/SAV\/mentions\/DEFAUT\/lieux\/RAM\/couleurs\/blanc\/cepages\/CHE"\)/;\1;"141"/' >> $TMP/sed_produit_code_cleaned
echo 's/;;\("\/declaration\/certifications\/AOC\/genres\/TRANQ\/appellations\/SAU\/mentions\/DEFAUT\/lieux\/CHA\/couleurs\/rouge\/cepages\/DEFAUT"\)/;\1;"182"/' >> $TMP/sed_produit_code_cleaned
echo 's/;;\("\/declaration\/certifications\/AUTRES\/genres\/DEFAUT\/appellations\/LIES\/mentions\/LBVE\/lieux\/DEFAUT\/couleurs\/DEFAUT\/cepages\/DEFAUT"\)/;\1;"0"/' >> $TMP/sed_produit_code_cleaned
echo 's/;;\("\/declaration\/certifications\/ALCOOL\/genres\/DEFAUT\/appellations\/CID\/mentions\/DEFAUT\/lieux\/DEFAUT\/couleurs\/DEFAUT\/cepages\/DEFAUT"\)/;\1;"0"/' >> $TMP/sed_produit_code_cleaned
echo 's/;;\("\/declaration\/certifications\/AOC_INTERLOIRE\/genres\/TRANQ\/appellations\/COA\/mentions\/DEFAUT\/lieux\/DEFAUT\/couleurs\/rose\/cepages\/CBF"\)/;\1;"0"/' >> $TMP/sed_produit_code_cleaned


echo "s/;;\(\"\/declaration\/[^\"]*\"\);/;\1;;/" >> $TMP/sed_produit_code_cleaned

# I - Récupération DS négoce/Caves Coop
echo "#DS;campagne;identifiant declarant;hash produit;code produit;periode;identifiant;volume stock;volume stock élaboration;vci;reserve_qualitative;nom declarant;libelle produit" > $TMP/export_bi_dss-nonwell.csv
curl -s http://$COUCHHOST:$COUCHPORT/$COUCHBASE/_design/ds/_view/stocks | sed -f bin/unicode2alpha | sed 's/.*"key":\[/DS;/' | sed 's/\],"value":\[*/,/' | sed 's/\]*\},*//' | grep '^DS;"' | sed 's/,/;/g' | sed 's/\r*$/;/' | sed 's/null//g' | sed 's/";;"/";"/' | sed 's/;"\/declaration/;\0/g' | sed -f $TMP/sed_produit_code_cleaned >> $TMP/export_bi_dss-nonwell.csv


# II - Récupération des DRMs et réduction de fichiers
curl -s http://$COUCHHOST:$COUCHPORT/$COUCHBASE/_design/drm/_view/stocks | sed -f bin/unicode2alpha | sed 's/.*"key":\[/DRM;/' | sed 's/\],"value":\[*/,/' | sed 's/\]*\},*//' | grep '^DRM;"' | sed 's/,/;/g' | sed 's/\r*$/;/' | sed 's/null//g' | sed 's/";;"/";"/' > $TMP/export_bi_drm_stock.csv
cat $TMP/export_bi_drm_stock.csv | awk -F ";" ' substr($5,2,4) >= 2017  { print }' | grep -Ev "(AOC_HORSINTERLOIRE|AOC_BIVC|IGP_HORSVALDELOIRE|VINSSIG)" > $TMP/export_bi_drm_stock_campagne_filtered.csv

# Récupération des plus hautes modificatrices

cat $TMP/export_bi_drm_stock_campagne_filtered.csv | awk  -F ";" '{ gsub("\"","",$3); gsub("\"","",$5); gsub("\"","",$6); print $3"-"$5"-"$6";"$1";"$2";"$3";"$4";"$5";"$6";"$7";"$8";"$9";"$10";"$11";"$12";"$13 }' | sed -r 's/^([0-9]*-[0-9]{6}-);/\1M00;/g' > $TMP/export_bi_drm_stock_campagne_filtered_with_modificatrices.csv
cat $TMP/export_bi_drm_stock_campagne_filtered_with_modificatrices.csv | cut -d ";" -f 1 | sort -r | uniq -w 16 > $TMP/export_bi_drm_ids.csv

# On garde uniquement les lignes de plus hautes modificatrices
rm $TMP/export_bi_drm_stock_last_modificatrices.csv > /dev/null 2>&1
touch $TMP/export_bi_drm_stock_last_modificatrices.csv

cat $TMP/export_bi_drm_stock_campagne_filtered_with_modificatrices.csv | while read line; do
   DOCID=`echo $line | cut -d ";" -f 1`;
   CMD=`cat $TMP/export_bi_drm_ids.csv | grep $DOCID`;
   if [ ! -z $CMD ]
   then
     echo $line >> $TMP/export_bi_drm_stock_last_modificatrices.csv
   fi
done

# III - Traitement pour les vitis Hors DRA hors COOP

# Récupération des coopératives
cat $TMP/export_bi_etablissements.csv | awk  -F ";" ' $3 == "\"COOPERATIVE\"" { gsub("(ETABLISSEMENT-)","",$5); print $5";"$3 }' > $TMP/export_cooperatives.csv

# On ne garde que les lignes d'aout
cat $TMP/export_bi_drm_stock_last_modificatrices.csv | grep -E ";[0-9]{4}08;" > $TMP/export_bi_drm_stock_campagne_filtered_aout.csv

# On enlève les coopératives
rm $TMP/export_bi_drm_stock_campagne_filtered_aout_without_coop.csv > /dev/null 2>&1
touch $TMP/export_bi_drm_stock_campagne_filtered_aout_without_coop.csv
cat $TMP/export_bi_drm_stock_campagne_filtered_aout.csv | while read line; do
   LINEETBID=`echo $line | cut -d ";" -f 4`;
   CMD=`cat $TMP/export_cooperatives.csv | grep $LINEETBID`;
   if [ -z $CMD ]
   then
     echo $line >> $TMP/export_bi_drm_stock_campagne_filtered_aout_without_coop.csv
   fi
done


# IV - Traitement pour les vitis DRA hors COOP

# On ne garde que les lignes de juillet
cat $TMP/export_bi_drm_stock_last_modificatrices.csv | grep -E ";[0-9]{4}07;" > $TMP/export_bi_drm_stock_campagne_filtered_juillet.csv

# On ne garde que les lignes de DRA

rm $TMP/export_bi_drm_stock_campagne_filtered_juillet_for_dra.csv > /dev/null 2>&1
touch $TMP/export_bi_drm_stock_campagne_filtered_juillet_for_dra.csv
cat $TMP/export_bi_drm_stock_campagne_filtered_juillet.csv | while read line; do
   LINEETBID=`echo $line | cut -d ";" -f 4`;
   ETBID="ETABLISSEMENT-"$LINEETBID;
   CMDCURL=`curl -s http://$COUCHHOST:$COUCHPORT/$COUCHBASE/$ETBID | sed -r 's/.*"type_dr":"([A-Z]{3})".*/\1/g'`;
   if [ "$CMDCURL" == "DRA" ]
   then
     echo $line >> $TMP/export_bi_drm_stock_campagne_filtered_juillet_for_dra.csv
   fi
done

# V - Mise en forme des données et ajout dans le fichiers stock

# Récupération de tout les libellés depuis les mouvements
curl -s http://$COUCHHOST:$COUCHPORT/$COUCHBASE/_design/mouvement/_view/consultation  | sed -f bin/unicode2alpha | sed 's/.*"key":\[/MOUVEMENT;/' | sed 's/\],"value":\[/,/' | sed 's/\]\},*//' | grep '^MOUVEMENT;"' | sed 's/,/;/g' | sed 's/\r*$/;/' | sed 's/null//g' | sed 's/;"\/declaration/;\0/g' > $TMP/export_bi_mouvements_for_match_libelle.csv

cat $TMP/export_bi_mouvements_for_match_libelle.csv | sed -r 's|\/details(ACQUITTE)?\/([a-zA-Z0-9]+)";|";|g' | awk -F ";" ' { print $8";"$14 }' | sort -u -t ";" -k1,1 | grep -E '".*";".*"' | awk -F ";" ' { gsub("/","#",$1); print "s/;\\("$1"\\)/;\\1;"$2"/" }' | sed 's/#/\\\//g' | sort | uniq  > $TMP/sed_produit_libelle.tmp

echo 's/;\("\/declaration\/certifications\/AUTRES\/genres\/DEFAUT\/appellations\/LIES\/mentions\/LBVE\/lieux\/DEFAUT\/couleurs\/DEFAUT\/cepages\/DEFAUT"\)/;\1;"Lies, Bourbes et vins à éliminer"/' >> $TMP/sed_produit_libelle.tmp
echo 's/;\("\/declaration\/certifications\/ALCOOL\/genres\/DEFAUT\/appellations\/CID\/mentions\/DEFAUT\/lieux\/DEFAUT\/couleurs\/DEFAUT\/cepages\/DEFAUT"\)/;\1;"Cidres, Poirés"/' >> $TMP/sed_produit_libelle.tmp
echo 's/;\("\/declaration\/certifications\/AOC_INTERLOIRE\/genres\/TRANQ\/appellations\/COA\/mentions\/DEFAUT\/lieux\/DEFAUT\/couleurs\/rose\/cepages\/CBF"\)/;\1;"Coteaux d'"'"'Ancenis Rosé Cabernet"/' >> $TMP/sed_produit_libelle.tmp

cat $TMP/sed_produit_libelle.tmp > $TMP/sed_produit_libelle
cat $TMP/sed_produit_libelle.tmp | sed 's|\\\/AOC\\\/|\\\/AOC_INTERLOIRE\\\/|g' >> $TMP/sed_produit_libelle


#cat $TMP/sed_produit_libelle | sort -u -t ";" -k2,2 | uniq > $TMP/sed_produit_libelle.tmp

#cat $TMP/sed_produit_libelle.tmp > $TMP/sed_produit_libelle

rm $TMP/export_bi_mouvements_for_match_libelle.csv;

# Transformation au format du fichier des Non-DRAs
cat $TMP/export_bi_drm_stock_campagne_filtered_aout_without_coop.csv | awk -F ";" ' { print "DS;\""substr($6,1,4)-1"-"substr($6,1,4)"\";\""$4"\";;"$5";\"DRM-"$1"\";\""$8"\";;;;"$14 }' | sed -f $TMP/sed_produit_code_cleaned | sed -f $TMP/sed_produit_libelle | sed 's/-M00//g' | sed -r 's|;("/declaration/[^\"]*");("[0-9]+");|;\1;;\2;|' > $TMP/export_bi_drm_stock_1.csv

cat $TMP/export_bi_drm_stock_1.csv | awk -F ";" ' { print $1";"$2";"$3";"$4";"$6";\""substr($2,7,4)"07\";"$7";"$8";"$9";"$10";"$11";"$12";"$5";" }' | grep -v "/declaration/certifications/IGP_HORSVALDELOIRE" > $TMP/export_bi_drm_final_for_non_dra.csv

# Transformation au format du fichier des DRAs
cat $TMP/export_bi_drm_stock_campagne_filtered_juillet_for_dra.csv  | awk -F ";" ' { print "DS;\""substr($6,1,4)-1"-"substr($6,1,4)"\";\""$4"\";;"$5";\"DRM-"$1"\";\""$13"\";;;;"$14 }' | sed -f $TMP/sed_produit_code_cleaned | sed -f $TMP/sed_produit_libelle | sed 's/-M00//g' | sed -r 's|;("/declaration/[^\"]*");("[0-9]+");|;\1;;\2;|' > $TMP/export_bi_drm_stock_2.csv

cat $TMP/export_bi_drm_stock_2.csv | awk -F ";" ' { print $1";"$2";"$3";"$4";"$6";\""substr($2,7,4)"07\";"$7";"$8";"$9";"$10";"$11";"$12";"$5";" }' | grep -v "/declaration/certifications/IGP_HORSVALDELOIRE" > $TMP/export_bi_drm_final_for_dra.csv


cat $TMP/export_bi_drm_final_for_non_dra.csv >> $TMP/export_bi_dss-nonwell.csv
cat $TMP/export_bi_drm_final_for_dra.csv >> $TMP/export_bi_dss-nonwell.csv


cat $TMP/export_bi_dss-nonwell.csv | awk -F ';' '{ isDrm=index($0,"DRM-"); code_produit=""; if(match($8,/^\"[0-9]+\"$/)){ code_produit=$8; } drmid=""; if(match($8,/^\"DRM-[0-9]+-[0-9]+.+\"/)){ drmid=$8; } if(match($9,/^\"DRM-[0-9]+-[0-9]+.+\"/)){ drmid=$9; } stock=""; if(match($9,/^\"[0-9.]+\"$/)){ stock=$9; } if(match($10,/^\"[0-9.]+\"$/)){ stock=$10; } if(isDrm == 0){ print $0; }else{ print $1";"$2";"$3";"$4";"code_produit";"$6";"drmid";"stock";;;;;"$13; } }' > $TMP/export_bi_dss.tmp.csv

rm $TMP/export_bi_dss.tmp2.csv > /dev/null 2>&1
touch $TMP/export_bi_dss.tmp2.csv

cat $TMP/export_bi_etablissements.csv | cut -d ";" -f 5,6 | sed 's|"||g' | sed 's|ETABLISSEMENT-||' > $TMP/identifiants_etablissements.csv

cat $TMP/export_bi_dss.tmp.csv | while read line; do
  IDENTIFIANTETB=`echo $line | cut -d ";" -f 3 | sed 's|"||g'`;
  NOMETB=`echo $line | cut -d ";" -f 12 | sed 's|"||g'`;
  if test -z "$NOMETB"
  then
    NOMETB=`grep -E "^$IDENTIFIANTETB;" $TMP/identifiants_etablissements.csv | cut -d ";" -f 2 | sed 's|"||g'`;
    F1=`echo $line | cut -d ";" -f 1,2,3,4,5,6,7,8,9,10,11`;
    F2=`echo $line | cut -d ";" -f 13`;
    echo $F1";\""$NOMETB"\";"$F2 >> $TMP/export_bi_dss.tmp2.csv;
  else
    echo $line >> $TMP/export_bi_dss.tmp2.csv;
  fi
done

cat $TMP/sed_produit_code | grep -Ev ';""/$' | sort | uniq | grep -v '\[^"\]' | cut -d ';' -f 3,5 | sed 's|"||g' | sed 's|\\(\\||' | sed 's|\\)\/||' | sed -r 's|\/$||' | sed 's|\\/|/|g' > $TMP/produit_code_stock

rm $TMP/export_bi_dss.tmp3.csv > /dev/null 2>&1
touch $TMP/export_bi_dss.tmp3.csv

cat $TMP/export_bi_dss.tmp2.csv | while read line; do
  PRODUITHASH=`echo $line | cut -d ";" -f 4 | sed 's|"||g'`;
  PRODUITCODE=`echo $line | cut -d ";" -f 5 | sed 's|"||g'`;
  if test -z "$PRODUITCODE"
  then
    PRODUITCODE=`grep -E "^$PRODUITHASH;" $TMP/produit_code_stock | cut -d ";" -f 2 | sed 's|"||g'`;
    F1=`echo $line | cut -d ";" -f 1,2,3,4`;
    F2=`echo $line | cut -d ";" -f 6,7,8,9,10,11,12,13`;
    echo $F1";\""$PRODUITCODE"\";"$F2 >> $TMP/export_bi_dss.tmp3.csv;
  else
    echo $line >> $TMP/export_bi_dss.tmp3.csv;
  fi
done

mv $TMP/export_bi_dss.tmp3.csv $TMP/export_bi_dss.csv

cat $TMP/export_bi_dss.csv | grep -E $SHORT > $TMP/export_bi_dss_short.csv

echo "FIN"
